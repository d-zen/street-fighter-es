import { controls } from '../../constants/controls';

let fighterOne, fighterTwo;

const PlayerOneCriticalHitCombination = controls['PlayerOneCriticalHitCombination'];
const PlayerTwoCriticalHitCombination = controls['PlayerTwoCriticalHitCombination'];

export function detectCritical(map) {
  const playerOneCrit = Object.keys(map).some( key => map[key] && (PlayerOneCriticalHitCombination.includes(key)));
  if (playerOneCrit) {
    return 'PlayerOneCriticalHitCombination';
  } else {
    return 'PlayerTwoCriticalHitCombination';
  }
}

export function checkCritical(map) {
  const keys = Object.keys(map);
  let res;
  if (keys.length >= 3) {
    res = Object.keys(map).some( key => map[key] && (PlayerOneCriticalHitCombination.includes(key) || PlayerTwoCriticalHitCombination));
  }
  return res;
}

export async function fight(firstFighter, secondFighter, hpIndicators) {
  return new Promise((resolve) => {
    setData(firstFighter, secondFighter, hpIndicators);

    let map = {};
    onkeydown = onkeyup = function(key){
      if (PlayerOneCriticalHitCombination.includes(key.code) || PlayerTwoCriticalHitCombination.includes(key.code)) {
        map[key.code] = key.type === 'keydown';
      }

      if (checkCritical(map)) {
        const criticalAction = detectCritical(map);
        performAttackAction(criticalAction);
        map = {};
      }

      if (key.type === 'keydown') {
        if (key && Object.values(controls).includes(key.code)) {
          const action = getAction(key.code);
          if (!isBlockAction(action)) {
            performAttackAction(action);
          } else {
            performBlockAction(action)
          }
        }

        if (fighterOne.winner || fighterTwo.winner) {
          return resolve(fighterOne.winner ? fighterOne : fighterTwo);
        }
      } else if (key.type === 'keyup') {
        if (key && Object.values(controls).includes(key.code)) {
          const action = getAction(key.code);
          if (isBlockAction(action)) {
            performBlockAction(action, true)
          }
        }
      }
    }
  });
}

const getAction = (keyPressed) => {
  return Object.keys(controls).find(key => controls[key] === keyPressed);
};

export function updateHP(fighter) {
  const width = fighter.health * 100 / fighter.nominalHealth;
  fighter.indicator.querySelector('.arena___health-bar').style.width = width > 0 ? `${width}%` : '0';
}

export function setBlock(fighter) {
  fighter.blocking = true;
}

export function isBlockAction(action) {
  return action && action === 'PlayerOneBlock' || action === 'PlayerTwoBlock';
}

export function removeBlock(fighter) {
  fighter.blocking = false;
}

export function performHit(provider, receiver, critical) {
  const damage = getDamage(provider, receiver, critical);

  if (damage > 0) {
    receiver.health -= damage;
    updateHP(receiver);
    console.info(provider.name + ' hits ' + receiver.name + ' on ' + damage + ' hp.');

    if (receiver.health <= 0) {
      provider.winner = true;
    }
  } else {
    console.info(receiver.name + ' dodged hit!');
  }
}

function calculateHit(provider, receiver, critical) {
  removeBlock(provider);
  if (critical) {
    if (!provider.disabledCritical) {
      provider.disabledCritical = true;
      setTimeout(() => {
        provider.disabledCritical = false;
      }, 10000);
      performHit(provider, receiver, critical);
    }
  } else {
    if (!receiver.blocking) {
      performHit(provider, receiver);
    } else {
      console.info(receiver.name, ' blocked!');
    }
  }
}

export async function performBlockAction(action, release) {
  switch (action) {
    case 'PlayerOneBlock':
      if (release) {
        removeBlock(fighterOne)
      } else {
        setBlock(fighterOne);
      }
      break;

    case 'PlayerTwoBlock':
      if (release) {
        removeBlock(fighterTwo)
      } else {
        setBlock(fighterTwo);
      }
      break;
  }
}

export async function performAttackAction(action) {
  switch (action) {
    case 'PlayerOneAttack':
      calculateHit(fighterOne, fighterTwo);
      break;

    case 'PlayerTwoAttack':
      calculateHit(fighterTwo, fighterOne);
      break;

    case 'PlayerOneCriticalHitCombination':
      calculateHit(fighterOne, fighterTwo, true);
      break;

    case 'PlayerTwoCriticalHitCombination':
      calculateHit(fighterTwo, fighterOne, true);
      break;
  }
}

function setData(firstFighter, secondFighter, hpIndicators) {
  // fighterOne = Object.assign(firstFighter, fighterOne);
  fighterOne = firstFighter;
  fighterOne.indicator = hpIndicators[fighterOne.name];
  fighterOne.nominalHealth = fighterOne.health;

  // fighterTwo = {...secondFighter};
  fighterTwo = secondFighter;
  fighterTwo.indicator = hpIndicators[fighterTwo.name];
  fighterTwo.nominalHealth = fighterTwo.health;
}

export function randomInt(min = 1, max = 2) {
  return min + Math.random() * (max - min);
}

export function getDamage(attacker, defender, critical) {
  if (critical) {
    return 2 * attacker.attack;
  }
  const attackPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = attackPower - blockPower;
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = randomInt();
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = randomInt();
  return fighter.defense * dodgeChance;
}
