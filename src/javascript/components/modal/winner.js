import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  const avatar = createFighterImage(fighter);
  const title = `${fighter.name} wins!`;
  const config = {title: title, bodyElement: avatar};
  showModal(config);
}
